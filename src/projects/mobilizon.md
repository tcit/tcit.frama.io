---
excerpt: Mobilizon est une application libre et fédérée pour organiser des événements.
start_date: "2018"
thumbnail: mobilizon_logo.png
date: "2020"
title: Mobilizon
end_date: "2022"
tags:
  - Elixir
  - ActivityPub
  - Typescript
  - VueJS
  - NodeJS
  - PostgreSQL
  - Fédération
  - Javascript
link: https://joinmobilizon.org
layout: static
path: "/mobilizon"
order: 1
---
**Mobilizon** doit nous permettre d'organiser la publication et l'organisation d'événements sans avoir à nous inscrire sur des plateformes propriétaires non respectueuses de nos données (Facebook, Meetup).

Le projet a débuté en 2018 et a fait l'objet d'un financement participatif en 2019. Il est prévu qu'une première version stable sorte courant 2020.

J'ai eu pour la première fois l'occasion de travailler avec deux designers, [Geoffrey Dorne](https://designandhuman.com/) et surtout [Marie-Cécile Godwin Paccard](https://mcgodwin.com/), designer et chercheuse UX dont la contribution apportée à ce projet est absolument énorme.

La particularité de ce projet est que la plateforme est fédérée, c'est-à-dire que différentes *instances* du logiciel peuvent être lancées en même temps et échanger des informations entre elles. Cela se fait à l'aide du protocole [ActivityPub](https://activitypub.rocks/), également utilisé par des projets comme [Mastodon](https://joinmastodon.org) ou [PeerTube](https://joinpeertube.org/).

Côté technique, j'ai fait le choix du langage [Elixir](https://elixir-lang.org/) pour le *backend*, [GraphQL](https://graphql.org/) pour l'API côté client et [VueJS](https://vuejs.org/) avec [Typescript](https://www.typescriptlang.org/) pour le *frontend*.