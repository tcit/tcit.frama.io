---
title: Nextcloud
start_date: "2016"
end_date: "2022"
excerpt: Nextcloud est une plateforme libre auto-hébergée pour garder le
  contrôle sur ses données.
link: https://nextcloud.com
tags:
  - Nextcloud
  - PHP
  - VueJS
  - Agenda
  - Framasoft
thumbnail: nextcloud-logo.webp
layout: static
path: "/nextcloud"
order: 2
---
Je me suis impliqué dans le projet **Nextcloud** durant un stage chez [Framasoft](https://framasoft.org), qui a donné le service [Framagenda](https://framagenda.org/). Avec l'aide d'autres contributeur·ices, j'ai ainsi ajouté les fonctionnalité de souscription à des agendas externes et de publication d'agendas via des liens accessibles publiquement.

Depuis, je suis co-mainteneur de l'application Agenda de Nextcloud et je contribue à Nextcloud régulièrement, surtout autour des fonctionnalités d'agendas.

J'ai également écrit quelques applications Nextcloud que nous utilisons à Framasoft&nbsp;:
* [Drop Account](https://framagit.org/framasoft/nextcloud/drop_account) Une application pour permettre aux utilisateur·ices de supprimer leur compte&nbsp;;
* [Login Notes](https://framagit.org/framasoft/nextcloud/login-notes) Une application pour permettre aux administrateur·ices de laisser des messages d'information sur la page de connexion.