---
title: Terre Brûlée
date: 2019-11-09 22:57:27
tags:
- Irlande
- Vacances
- Photos
categories:
- Personnel
thumbnailImage: https://cloud.tcit.fr/s/qq49QbdxLCZrmTx/preview
thumbnailImagePosition: left
photos:
- src: https://cloud.tcit.fr/s/xcTtiH38iF3Hekp/preview
  address: Castle Ward, Strangford, Downpatrick BT30 7BA, Royaume-Uni
  comment: Certain·es personnes s'y rendent pour le lieu de tournage de certaines scènes de Game of Thrones, mais le lieu sera décevant vis-à-vis de vos attentes, donc autant y aller pour autre chose.
- src: https://cloud.tcit.fr/s/rnMFf8PdD6j8DmY/preview
  address: Castlewellan, Royaume-Uni
  comment: Cette hôte était plutôt généreuse sur les *scones* lorsque nous sommes arrivés.
- src: https://cloud.tcit.fr/s/qcA8six3cczEFWi/preview
  address: Tollymore Forest Park, Bryansford Rd, Newcastle BT33 0PR, Royaume-Uni
  comment: L'arbre blanc. Provient peut-être de Mindolluin.
- src: https://cloud.tcit.fr/s/BkSXRBb4tfjGCs4/preview
  address: Torr Head, Ballycastle BT54 6RQ, Royaume-Uni
  comment: Temps typiquement irlandais. Au large, les côtes écossaises.
- src: https://cloud.tcit.fr/s/qq49QbdxLCZrmTx/preview
  address: Château de Dunluce, Bushmills BT57 8UY, Royaume-Uni
  comment: J'ai un gros doute sur le lieu de prise de vue de cet agneau.
- src: https://cloud.tcit.fr/s/p8fdZeYHKs743yj/preview
  address: Downpatrick Head, Knockaun, Ballycastle, Co. Mayo, Irlande
- src: https://cloud.tcit.fr/s/Lbbb9zfxD2QTzT9/preview
  comment: J'adore absolument ces boules d'herbe.
- src: https://cloud.tcit.fr/s/CSLEz8cdZb5MSme/preview
  address: Knocknarea, Strandhill, Co. Sligo, Irlande
  comment: Il faut monter pour avoir la vue, mais ça vaut le coup.
- src: https://cloud.tcit.fr/s/jdebLY8Kx5zJmFL/preview
  address: Achill Island, Co. Mayo, Irlande
  comment: Meilleur endroit au monde avec la meilleure météo possible. On aurait pu y passer des semaines.
- src: https://cloud.tcit.fr/s/F383tjACSTtzkio/preview
  address: Keem Bay, Keel West, Keem, Co. Mayo, Irlande
  comment: Vous avez dit Irlande ? Bon, certes, elle était froide.
- src: https://cloud.tcit.fr/s/fXcqTLb49xyNmzo/preview
  address: Doolough Valley, Co. Mayo, Irlande
  comment: La superbe route soudainement montagnarde qui se termine dans le fjord de Killary.
- src: https://cloud.tcit.fr/s/NJo7g3ssqqTTmAJ/preview
  address: Diamond Hill, Parc National du Connemara, Co. Galway, Irlande
  comment: Encore une fois, il faut monter pas mal.
- src: https://cloud.tcit.fr/s/y3XXrzQMw39EgkS/preview
- src: https://cloud.tcit.fr/s/waMfHtLLZoj2GHo/preview
  address: Black Head, Co. Clare, Irlande
  comment: La pierre est superbe.
- src: https://cloud.tcit.fr/s/tMbfgsbY7Zy2JmM/preview
- src: https://cloud.tcit.fr/s/Aa596qZkqR3EnwG/preview
- src: https://cloud.tcit.fr/s/THXK2RnJwiqZ9GG/preview
  address: Falaises de Moher, Co. Clare, Irlande
layout: post
---
Au vent, des landes de pierre…
Maintenant que cette chanson est en tête, je voulais juste partager quelques photos d'Irlande.

<!--more-->

J'y étais en Juin, en *road trip* sur la plus grande partie du littoral de la moitié Nord.
Voici des photos de mes coins préférés, surtout dans le comté de Mayo, de Downpatrick Head à la vallée de Doolough, en passant par l'île d'Achill et en poussant jusqu'au parc national du Connemara.

Elles sont classées par ordre chronologique.

